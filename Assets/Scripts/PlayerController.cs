﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour
{


    [SerializeField] float valueToMakePlayerMove = 0.2f;
    [SerializeField] Joystick moveJoyStick;
    Player player;
    public bool isClimbing;

    private void Start()
    {
        player = GetComponentInParent<Player>();
    }

    private void Update()
    {
        
        if (!player.isPlayerDead && player.isPlayerAbleToMove)
        {
            Climb();
            Run();
           
        }
    }



    public void Dash()// this is called though buttons on the Player Controls UI canvas
    {
        if(isClimbing){return;}
        if (player.canDash == false) { return; }
        var direction = Mathf.Sign(moveJoyStick.Horizontal);
        if(player.transform.localScale.x == 1)// to find which way the player is looking 
        {
            player.transform.position += new Vector3(player.dashDistance, 0, 0);
        }
        else if (player.transform.localScale.x == -1)
        {
            player.transform.position += new Vector3(-player.dashDistance, 0, 0);
        }
        StartCoroutine(DashCoolDown(player.dashCoolDownTime));
    }

    IEnumerator DashTime(float time)
    {
        StartCoroutine(DashCoolDown(player.dashCoolDownTime));
        yield return new WaitForSeconds(time);
    }

    IEnumerator DashCoolDown(float time)
    {
        player.canDash = false;
        StartCoroutine(FindObjectOfType<GameSession>().StartCountdown(time));
        yield return new WaitForSeconds(time);
        player.canDash = true;
    }

    private void Run()
    {
        if (moveJoyStick.Horizontal < -valueToMakePlayerMove )
        {
            MovePlayer();
        }
        else if (moveJoyStick.Horizontal > valueToMakePlayerMove)
        {
            MovePlayer();
        }
   
    }

    private void MovePlayer()
    {
        if (player.GetPlayerRidgidBody().velocity.x >= player.playerMaxVelocity)
        {
            player.GetPlayerRidgidBody().velocity = new Vector2(player.playerMaxVelocity, player.myRigidBody.velocity.y);
        }
        else if (player.GetPlayerRidgidBody().velocity.x <= -player.playerMaxVelocity)
        {
            player.GetPlayerRidgidBody().velocity = new Vector2(-player.playerMaxVelocity, player.myRigidBody.velocity.y);
        }
        float controlThrow = moveJoyStick.Horizontal;
        Vector2 playerVelocity = new Vector2(controlThrow * player.playerAcceleration, 0);
        player.AddForceToPlayer(playerVelocity);
    }

    public void Jump()// this is called though buttons on the Player Controls UI canvas
    {
        if (player.isPlayerDead || player.isPlayerAbleToMove == false) { return; }
        //Can be used to know if the player is touching the "Ground" Layer
        /*if (!myCollider2D.IsTouchingLayers(LayerMask.GetMask("Ground")))
        {
            return;
        }*/
        if (player.numOfJumps <= 0)
        {
            return;
        }
        else
        {
            player.GetPlayerRidgidBody().velocity = new Vector2(player.GetPlayerRidgidBody().velocity.x, player.jumpHight);
            player.TakeJump();
        }

       
    }

    private void Climb()
    {
        if (!player.myBodyCollider2D.IsTouchingLayers(LayerMask.GetMask("Ladders")) || player.myFeetCollider2D.IsTouchingLayers(LayerMask.GetMask("ForeGround")))
        {
            isClimbing = false;
           player.myRigidBody.gravityScale = 1;
            player.myAnimator.SetBool("IsClimbing", false);
            return;
        }
        player.RefreshJumps();
        bool playerHasVerticalSpeed = Mathf.Abs(player.myRigidBody.velocity.y) > Mathf.Epsilon;
        if (!playerHasVerticalSpeed)
        {
            player.myAnimator.SetBool("IsClimbing", false);
        }
        else if(player.GetPlayerRidgidBody().velocity.x > 0 ||playerHasVerticalSpeed && !player.myFeetCollider2D.IsTouchingLayers(LayerMask.GetMask("ForeGround")))
        {
            isClimbing = true;
            player.myAnimator.SetBool("IsClimbing", true);
        }

        float controlThrows = moveJoyStick.Vertical;
        float controlThrowsX = moveJoyStick.Horizontal;

        Vector2 climbVelocity = new Vector2(controlThrowsX, controlThrows * player.climbSpeed);
        player.myRigidBody.velocity = climbVelocity;
        player.myRigidBody.gravityScale = 0;
    }

    void Punch()
    {

    }

}
