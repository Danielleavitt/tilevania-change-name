﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPickUp : MonoBehaviour
{
    [SerializeField] int CoinValue = 10;
    [SerializeField] AudioClip pickUpSound;
    [SerializeField] float clipVolume = 0.8f;
    private void OnTriggerEnter2D(Collider2D collision)
    {

        AudioSource.PlayClipAtPoint(pickUpSound, Camera.main.transform.position, clipVolume);
        FindObjectOfType<GameSession>().AddScore(CoinValue);
        Destroy(gameObject);

    }

}
