﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField] float movementSpeed = 10f;
    Rigidbody2D myRigidbody2D;
   




    void Start()
    {
        
        myRigidbody2D = GetComponent<Rigidbody2D>();
        
    }

    
    void Update()
    {
        if (IsFacingRight())
        {
            myRigidbody2D.velocity = new Vector2(movementSpeed, myRigidbody2D.velocity.y);
        }
        else
        {
            myRigidbody2D.velocity = new Vector2(-movementSpeed, myRigidbody2D.velocity.y);
        }

        
    }

    bool IsFacingRight()
    {
        return transform.localScale.x >= 0; 
    }

    

    private void OnTriggerExit2D(Collider2D collision)
    {
        transform.localScale = new Vector2(-(Mathf.Sign(myRigidbody2D.velocity.x)), 1f);
    }
}
