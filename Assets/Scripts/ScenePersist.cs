﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenePersist : MonoBehaviour
{

   int numOfScenemPersists;
    int startScene;

    private void Awake()
    {
         startScene = SceneManager.GetActiveScene().buildIndex;
    }
    void Start()
    {
        numOfScenemPersists =  FindObjectsOfType<ScenePersist>().Length;
        if(numOfScenemPersists > 1)
        {
            Destroy(gameObject);
        }
        else
        {
           
        }
    }

   

    // Update is called once per frame
    void Update()
    {
        if (startScene == SceneManager.GetActiveScene().buildIndex)
        {
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
