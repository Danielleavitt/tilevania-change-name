﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("Player Movement")]
    public float playerMaxVelocity = 20f;
    public float playerAcceleration = 20f;
    public float jumpHight = 5f;
    public float climbSpeed = 5f;
    [Header("Player Values")]
    [SerializeField] float playerHealth = 100;
    public int numOfJumps = 2;
    public float dashCoolDownTime = 2f;
    public float dashForce = 1000f;
    
    public float dashDistance = 5;

    //todo change values to get from enemy
    
    [SerializeField] float damageColorLength;
    [Header("Later to me found on enemy")]
    [SerializeField] float damageStaggerForce = 1000f;
    [SerializeField] float stunTime = 1f;


    [Header("Bool's")]
    public bool canDash = true;
    public bool isPlayerAbleToMove = true;
    public bool isPlayerDead;
    int originalJumpAmount;

    [HideInInspector]public Rigidbody2D myRigidBody;
    SpriteRenderer mySpriteRenderer;
    [HideInInspector]public Animator myAnimator;
    [HideInInspector]public CapsuleCollider2D myBodyCollider2D;
    [HideInInspector]public BoxCollider2D myFeetCollider2D;


    private void Start()
    {
        
        isPlayerDead = false;
        originalJumpAmount = numOfJumps;
        myRigidBody = GetComponent<Rigidbody2D>();
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        myAnimator = GetComponent<Animator>();
        myBodyCollider2D = GetComponent<CapsuleCollider2D>();
        myFeetCollider2D = GetComponent<BoxCollider2D>();
    }

    public void AddForceToPlayer(Vector2 vector2)
    {
        myRigidBody.AddForce(vector2);

    }

    public Rigidbody2D GetPlayerRidgidBody()
    {
        return GetComponent<Rigidbody2D>();
    }



    void Update()
    {

        if (!isPlayerDead && isPlayerAbleToMove)
        {

        }
        FlipSprite();

        //if (myRigidBody.velocity.x > playerMaxVelocity) { return; }
        ////{
        ////    myRigidBody.velocity = new Vector2(playerMaxVelocity, myRigidBody.velocity.y);
        ////}
        //if (myRigidBody.velocity.x < -playerMaxVelocity) { return; }
        //{
        //    myRigidBody.velocity = new Vector2(-playerMaxVelocity, myRigidBody.velocity.y);
        //}

    }

    

   


    private void FlipSprite()
    {
        // this bool is to find if there is horizontal speed.
        bool playerHasHorizontalSpeed = Mathf.Abs(myRigidBody.velocity.x) > Mathf.Epsilon;
        
        if (playerHasHorizontalSpeed && !GetComponentInChildren<PlayerController>().isClimbing)
        {
            transform.localScale = new Vector2(Mathf.Sign(myRigidBody.velocity.x), 1f);
            myAnimator.SetBool("IsRunning", true);
        }
        else
        {
            myAnimator.SetBool("IsRunning", false);
        }
    }

   

    private void OnCollisionEnter2D (Collision2D collision) 
    {
        if (myFeetCollider2D.IsTouchingLayers(LayerMask.GetMask("Ground")))
        {
            RefreshJumps();
        }


            if (isPlayerDead == false)
            {
                if (myBodyCollider2D.IsTouchingLayers(LayerMask.GetMask("Enemy")) && collision.gameObject.GetComponent<Damage>())//Enemy needs to be enemy layer and have a damage script to hurt player
                {
                    TakeDamage(collision.gameObject.GetComponent<Damage>().damage);
                }
                if (myFeetCollider2D.IsTouchingLayers(LayerMask.GetMask("Enemy"))&& collision.gameObject.GetComponent<Damage>())
                {
                    TakeDamage(collision.gameObject.GetComponent<Damage>().damage);

                }
            }
            
        
        
        
       
    }

    

   

    public void TakeDamage(float damage)
    {
        //if facing left applies force right
        if(gameObject.transform.localScale.x == -1)//todo change so the force is applyed by which side the enemy is 
        {
            print("force added right");
            myRigidBody.AddForce(new Vector2(damageStaggerForce, damageStaggerForce));
            StartCoroutine(DisableMoveForTime(stunTime));
        }
        //if facing left applies force left 
        else if (gameObject.transform.localScale.x == 1)
        {
            print("force added left");
            myRigidBody.AddForce(new Vector2(-damageStaggerForce, damageStaggerForce));
            StartCoroutine(DisableMoveForTime(stunTime));
        }
        StartCoroutine(ChangeColor());

        playerHealth = playerHealth - damage;
        if (playerHealth <= 0)
        {

            isPlayerDead = true;
            PlayerDead();

        }
    }

    IEnumerator DisableMoveForTime(float time)
    {
        isPlayerAbleToMove = false;
        yield return new WaitForSeconds(time);
        isPlayerAbleToMove = true;
    }

    IEnumerator ChangeColor()
    {
        print("Change Colr");
        GetComponent<SpriteRenderer>().color = Color.red;
        yield return new WaitForSeconds(damageColorLength);
        GetComponent<SpriteRenderer>().color = Color.white;

    }

    public void PlayerDead()
    {
        myAnimator.SetTrigger("IsDead");
        FindObjectOfType<GameSession>().ProcessPlayerDeath();
    }

    public void TakeJump()
    {
        numOfJumps--;
    }


    public void RefreshJumps()
    {
        numOfJumps = originalJumpAmount;
    }

   

}
