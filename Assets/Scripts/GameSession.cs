﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;



public class GameSession : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI livesText;
    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] TextMeshProUGUI dashDurationText;
    [SerializeField] TextMeshProUGUI playerVelocityText;
    [SerializeField] int numOfPlayerLives = 3;
    [SerializeField] float playerDeathDelay = 3;
    [SerializeField] int amountOfCoins = 0;
    int originalNumOfLives;

    private void Awake()
    {
        int numGameSessions = FindObjectsOfType<GameSession>().Length;
        if(numGameSessions > 1)
        {
            
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
           
        }
      
    }

    private void Update()
    {
        playerVelocityText.text = FindObjectOfType<Player>().GetPlayerRidgidBody().velocity.ToString();
    }


    public IEnumerator StartCountdown(float countdownValue )
    {
        float currCountdownValue;
        currCountdownValue = countdownValue;
        while (currCountdownValue > 0)
        {
            dashDurationText.text = currCountdownValue.ToString();
            yield return new WaitForSeconds(1.0f);
            currCountdownValue--;
            dashDurationText.text = currCountdownValue.ToString();
        }
    }

    private void Start()
    {
        livesText.text = numOfPlayerLives.ToString();
        scoreText.text = amountOfCoins.ToString();
    }


    public void ProcessPlayerDeath()
    {
        numOfPlayerLives = numOfPlayerLives - 1;
        
       
        livesText.text = numOfPlayerLives.ToString();
        StartCoroutine(PlayerDeathDelay());

    }

    IEnumerator PlayerDeathDelay()
    {
        yield return new WaitForSecondsRealtime(playerDeathDelay);
       
        if (numOfPlayerLives <= 0)
        {
            FindObjectOfType<LevelLoader>().LoadMainMenu();
            
            Destroy(gameObject);
        }
        else
        {
            FindObjectOfType<LevelLoader>().ReloadLevel();
            
        }
    }

    public int GetNumberOfLives()
    {
        return numOfPlayerLives;
    }

    public void AddScore(int coinAmount)
    {
        amountOfCoins = amountOfCoins + coinAmount;
        scoreText.text = amountOfCoins.ToString();
    }

   

}
